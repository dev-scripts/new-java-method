#!/bin/bash

#The purpose of this script is to make a new java methods with the formatting and javadoc already filled in.

#read -p "Enter name of new .java file, omit the '.java' extension: " name
name=$1
methodName=$2
returnType=$3
purpose=$4
echo "" >> $name.java
echo "    /**" >> $name.java
echo "    * $purpose" >> $name.java
echo "    */" >> $name.java
echo "    public static $returnType $methodName() {" >> $name.java
echo "        " >> $name.java
echo "        " >> $name.java
echo "        " >> $name.java
echo "    }" >> $name.java
